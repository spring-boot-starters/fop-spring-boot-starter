package com.gitlab.spring.boot.starter.fop.configuration;

public class FopWriterException extends RuntimeException {
    public FopWriterException(Exception e) {
        super(e);
    }
}
