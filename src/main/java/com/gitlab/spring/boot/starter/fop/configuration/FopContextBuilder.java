package com.gitlab.spring.boot.starter.fop.configuration;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

public final class FopContextBuilder {

    public static FopContext newFopContext(InputStream data,
                                           InputStream schema,
                                           OutputStream outputStream) {
        return new FopContextImpl(data, schema, outputStream);
    }

    public static FopContext newFopContext(String dataFileName,
                                           String schemaFileName,
                                           String outputFileName) throws FileNotFoundException {
        return new FopContextImpl(dataFileName, schemaFileName, outputFileName);
    }


    private FopContextBuilder() {

    }

}
